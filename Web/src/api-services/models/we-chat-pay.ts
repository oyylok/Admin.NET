/* tslint:disable */
/* eslint-disable */
/**
 * Admin.NET
 * 让 .NET 开发更简单、更通用、更流行。前后端分离架构(.NET6/Vue3)，开箱即用紧随前沿技术。<br/><a href='https://gitee.com/zuohuaijun/Admin.NET/'>https://gitee.com/zuohuaijun/Admin.NET</a>
 *
 * OpenAPI spec version: 1.0.0
 * Contact: 515096995@qq.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { WeChatUser } from './we-chat-user';
/**
 * 微信支付表
 * @export
 * @interface WeChatPay
 */
export interface WeChatPay {
    /**
     * 雪花Id
     * @type {number}
     * @memberof WeChatPay
     */
    id?: number;
    /**
     * 创建时间
     * @type {Date}
     * @memberof WeChatPay
     */
    createTime?: Date | null;
    /**
     * 更新时间
     * @type {Date}
     * @memberof WeChatPay
     */
    updateTime?: Date | null;
    /**
     * 创建者Id
     * @type {number}
     * @memberof WeChatPay
     */
    createUserId?: number | null;
    /**
     * 修改者Id
     * @type {number}
     * @memberof WeChatPay
     */
    updateUserId?: number | null;
    /**
     * 软删除
     * @type {boolean}
     * @memberof WeChatPay
     */
    isDelete?: boolean;
    /**
     * 微信商户号
     * @type {string}
     * @memberof WeChatPay
     */
    merchantId?: string | null;
    /**
     * 服务商AppId
     * @type {string}
     * @memberof WeChatPay
     */
    appId?: string | null;
    /**
     * 商户订单号
     * @type {string}
     * @memberof WeChatPay
     */
    outTradeNumber?: string | null;
    /**
     * 支付订单号
     * @type {string}
     * @memberof WeChatPay
     */
    transactionId?: string | null;
    /**
     * 交易类型
     * @type {string}
     * @memberof WeChatPay
     */
    tradeType?: string | null;
    /**
     * 交易状态
     * @type {string}
     * @memberof WeChatPay
     */
    tradeState?: string | null;
    /**
     * 交易状态描述
     * @type {string}
     * @memberof WeChatPay
     */
    tradeStateDescription?: string | null;
    /**
     * 付款银行类型
     * @type {string}
     * @memberof WeChatPay
     */
    bankType?: string | null;
    /**
     * 订单总金额
     * @type {number}
     * @memberof WeChatPay
     */
    total?: number;
    /**
     * 用户支付金额
     * @type {number}
     * @memberof WeChatPay
     */
    payerTotal?: number | null;
    /**
     * 支付完成时间
     * @type {Date}
     * @memberof WeChatPay
     */
    successTime?: Date | null;
    /**
     * 交易结束时间
     * @type {Date}
     * @memberof WeChatPay
     */
    expireTime?: Date | null;
    /**
     * 商品描述
     * @type {string}
     * @memberof WeChatPay
     */
    description?: string | null;
    /**
     * 场景信息
     * @type {string}
     * @memberof WeChatPay
     */
    scene?: string | null;
    /**
     * 附加数据
     * @type {string}
     * @memberof WeChatPay
     */
    attachment?: string | null;
    /**
     * 优惠标记
     * @type {string}
     * @memberof WeChatPay
     */
    goodsTag?: string | null;
    /**
     * 结算信息
     * @type {string}
     * @memberof WeChatPay
     */
    settlement?: string | null;
    /**
     * 回调通知地址
     * @type {string}
     * @memberof WeChatPay
     */
    notifyUrl?: string | null;
    /**
     * 备注
     * @type {string}
     * @memberof WeChatPay
     */
    remark?: string | null;
    /**
     * 微信OpenId标识
     * @type {string}
     * @memberof WeChatPay
     */
    openId?: string | null;
    /**
     * 
     * @type {WeChatUser}
     * @memberof WeChatPay
     */
    weChatUser?: WeChatUser;
    /**
     * 子商户号
     * @type {string}
     * @memberof WeChatPay
     */
    subMerchantId?: string | null;
    /**
     * 子商户AppId
     * @type {string}
     * @memberof WeChatPay
     */
    subAppId?: string | null;
    /**
     * 子商户唯一标识
     * @type {string}
     * @memberof WeChatPay
     */
    subOpenId?: string | null;
}
